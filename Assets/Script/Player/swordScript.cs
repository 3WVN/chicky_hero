﻿using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;

public class swordScript : MonoBehaviour
{
    private float timeConter = 0;

    void Start()
    {
        SwordForce();
    }

    // Update is called once per frame
    void Update()
    {
        SwordDestroy();
    }

    void SwordForce()
    {
        Rigidbody2D swordRigidbody;
        swordRigidbody = GetComponent<Rigidbody2D>();
        
        if (playerController.isLookBack == false)
        {
            swordRigidbody.AddTorque(-30,0);
            swordRigidbody.AddForce(transform.up * 135);
            swordRigidbody.AddForce(transform.right * 135);
        }
        
        else if (playerController.isLookBack == true)
        {
            swordRigidbody.AddTorque(30,0);
            swordRigidbody.AddForce(transform.up * 135);
            swordRigidbody.AddForce(transform.right * -135);
        }
    }

    void SwordDestroy()
    {
        timeConter += Time.deltaTime;
        if (timeConter > 3)
        {
            Destroy(this.gameObject);
            
        }
    }

}
